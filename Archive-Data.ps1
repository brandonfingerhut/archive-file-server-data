[CmdletBinding()]
param(
    [Parameter(Mandatory=$false)]
    [String] $Config = "config-archive.xml"
)

$QuestionMark = '?'
$ArchivedTag = "_ARCHIVED"

[xml]$ConfigXML = Get-Content $Config
$DataToArchiveCsvPath = $ConfigXML.Configuration.Option | Where-Object {$_.Name -like 'DataToArchiveCsvPath'} | Select-Object -ExpandProperty 'Value'
$BlobUrl = $ConfigXML.Configuration.Option | Where-Object {$_.Name -like 'BlobUrl'} | Select-Object -ExpandProperty 'Value'
$SasToken = $ConfigXML.Configuration.Option | Where-Object {$_.Name -like 'SasToken'} | Select-Object -ExpandProperty 'Value'


$DataToArchive = Get-Content "$DataToArchiveCsvPath" | ConvertFrom-Csv

Foreach ($Item in $($DataToArchive.FullName)) {
    $ItemPath = $Item -replace "\\", "/"
    Invoke-Command {.\azcopy.exe cp $Item "$BlobUrl/$ItemPath$QuestionMark$SasToken"}
    $ItemAttributes = Get-Item -Path $ItemPath
    $ItemName = $ItemAttributes.Name
    $ItemBaseName = $ItemAttributes.BaseName
    $ItemExtension = $ItemAttributes.Extension
    switch ($ItemExtension) {
        .pdf {
            Copy-Item ".\Archive Templates\template.pdf" "$(Split-Path $ItemPath -Parent)\$ItemBaseName$ArchivedTag$ItemExtension" -Force
            Remove-Item $ItemPath
        }
        .xlsx {
            Copy-Item ".\Archive Templates\template.xlsx" "$(Split-Path $ItemPath -Parent)\$ItemBaseName$ArchivedTag$ItemExtension" -Force
            Remove-Item $ItemPath
        }

    }
}

<# Include This for Error Checking

$SASOutput = D:\DumpTools\azcopy.exe copy --log-level $AzCopyLogValue ($CrashDumpZIPpath + "" + $NameZipFile) ($uri + $NameZipFile + $SASToken) --check-length=false
$SASOutput
$ErrorSAS = $SASOutput -match 'ERROR'

     if( $SASOutput -match 'ERROR')
    {
       Write-Host "An error occurred [SAS] : $ErrorSAS"
       SendEmailNotification -EMAILTO "$SupportEmail" -EMAILSUBJECT "CrashDUMP [ ERROR ] Notification // $env:COMPUTERNAME" -EMAILBODY "An error occurred [CrashDUMP] : $ErrorSAS"
       Write-Host "Email notification has been successfully sent!" -BackgroundColor red -ForegroundColor white

       return
    }
    else {
        Write-Host "[SAS] Continue..." -ForegroundColor Magenta
    }



#>




<#

# How to set to Archive in Azure Storage

$StgAcc = "serverdataarchive"
$StgKey = "VflG1OOstt+vwfNGvr8KHmYvSwT4BmMzMtPUo1IA8sLjlSRUEwkq0bJ0xAkq/6agkTHrpV65NBaazx0wOFkybA=="
$Container = "sgr-s-fp-01"
$ctx = New-AzureStorageContext -StorageAccountName $StgAcc -StorageAccountKey $StgKey

#Get all the blobs in container
$blob = Get-AzureStorageBlob -Container $Container -Context $ctx

#Set tier of all the blobs to Archive
$blob.icloudblob.setstandardblobtier("Archive")

#>