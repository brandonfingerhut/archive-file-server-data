<#
.DESCRIPTION
Create CSV Export of files found at a given path that meet the archive criteria

.NOTES
[Change Log]
2021-12-14 - 0.1.0
Renamed to SearchFor-DataToArchive.ps1

2021-12-14 - 0.2.0
Corrected issue with compiling total amount to archive

.EXAMPLE
Run using default config file [config-search.xml]
.\SearchFor-DataToArchive.ps1

Run using custom config file
.\SearchFor-DataToArchive.ps1 -Config .\myownconfig.xml

#>

[CmdletBinding()]
param(
    [Parameter(Mandatory=$false)]
    [String] $Config = "config-search.xml"
)

$ScriptVersion = '0.2.0'

# Read in XML Configuration Variables
[xml]$ConfigXML = Get-Content $Config
$SearchPath = $ConfigXML.Configuration.Option | Where-Object {$_.Name -like 'SearchPath'} | Select-Object -ExpandProperty 'Value'
$OutputPath = $ConfigXML.Configuration.Option | Where-Object {$_.Name -like 'OutputPath'} | Select-Object -ExpandProperty 'Value'
$DaysSinceLastAccess = $ConfigXML.Configuration.Option | Where-Object {$_.Name -like 'DaysSinceLastAccess'} | Select-Object -ExpandProperty 'Value'

$ArchiveDataSums = @()

Write-Host "Script Version [$ScriptVersion]"
Write-Host "Search Path [$SearchPath]"
Write-Host "Output Path [$OutputPath]"
Write-Host "Days Since Last Access [$DaysSinceLastAccess]"

# Test for OutputPath, if it does not exist, create it
if (-not(Test-Path -Path $OutputPath)) {
    New-Item -Path (Split-Path -Path $OutputPath -Parent) -Name (Split-Path -Path $OutputPath -Leaf) -ItemType Directory -Force
}

function New-ReportOnDirectory() {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$DirectoryPath,
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [int]$Days
    )
    begin {
        # Set VerbosePreference to Continue so that verbose messages are displayed.
        $VerbosePreference = 'Continue'
        $DirectoryName = Split-Path -Path $DirectoryPath -Leaf
        $ReportName = "$DirectoryName.csv"
    }
    process {
        if (-not (Test-Path Eval:)) {
            Write-Verbose "Mounting virtual drive [Eval:] for Path [$DirectoryPath]"
            New-PSDrive -Name Eval -PSProvider FileSystem -Root $DirectoryPath | Out-Null
        }
        Get-ChildItem -LiteralPath "Eval:" -Recurse `
        | Where-Object {($_.Mode -notlike 'd') -and ($_.LastAccessTime -lt (Get-Date).AddDays(-$Days))} `
        | Select-Object Name, DirectoryName, FullName, @{Name="Size (KB)";Expression={ "{0:N0}" -f ($_.Length / 1KB) }}, Extension, CreationTimeUtc, LastAccessTimeUtc, LastWriteTimeUtc `
        | Export-Csv -NoTypeInformation -Path "$OutputPath\$ReportName" -Force
        $ArchiveDataSum = (Get-Content "$OutputPath\$ReportName" | ConvertFrom-Csv | Measure-Object "Size (KB)" -Sum).Sum
        $ArchiveDataSum = [math]::Round($($ArchiveDataSum / 1KB),2)
        # Write-Host "Directory [$Directory] total data to archive: $ArchiveDataSum MBs"
        if (Test-Path Eval:) {
            Write-Verbose "Un-mounting virtual drive Eval:"
            Remove-PSDrive -Name Eval | Out-Null
        }
        
    }
    end {
        return $ArchiveDataSum
    }
}

$Directories = Get-ChildItem -Path $SearchPath | Where-Object {($_.Mode -like 'd*')}
$ArchiveDataSums = @(foreach ($Directory in $Directories) {
    New-ReportOnDirectory -DirectoryPath $($Directory.FullName) -Days $DaysSinceLastAccess
})

$TotalArchiveDataSum = [math]::Round(($ArchiveDataSums | Measure-Object -Sum).Sum / 1KB)
Write-Host "Total Archive Data Sum [$TotalArchiveDataSum] MBs"
Set-Content "Total Archive Data Sum [$TotalArchiveDataSum] MBs" -Path "$OutputPath\ArchiveReport.html"
