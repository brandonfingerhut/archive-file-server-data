# Initialize these variables with your values.
$rgName = "ArchiveRG"
$accountName = "serverdataarchive"
$containerName = "mne-s-rodc-01"

# Get the storage account context
$ctx = (Get-AzStorageAccount `
        -ResourceGroupName $rgName `
        -Name $accountName).Context

# Change the blob's access tier to Archive
$BlobList = Get-AzStorageBlob -Container $containerName -Context $ctx
$BlobList.BlobClient.SetAccessTier("Archive")

